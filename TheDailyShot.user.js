// ==UserScript==
// @name            TheDailyShot
// @downloadURL     https://gitlab.com/zodiacmcfly/thedailyshotjs/-/raw/main/TheDailyShot.user.js
// @updateURL       https://gitlab.com/zodiacmcfly/thedailyshotjs/-/raw/main/TheDailyShot.user.js
// @match           *://thedailyshot.com/*
// @match           *://www.thedailyshot.com/*
// ==/UserScript==

(function() {
    'use strict';

    var jq = document.createElement('script');
    jq.src = "https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js";
    document.getElementsByTagName('head')[0].appendChild(jq);
    jQuery.noConflict();
    jQuery(".entry-content").show();

})();
